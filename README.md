# Demo train, select and serve a model

This is a project template that can be used to start a demo project. This AskAnna demo shows how you can:

- train multiple models
- select the best model
- use the best model to serve a prediction.

The `askanna.yml` contains the configuration for running the Python scripts. You can run the above jobs, and check the
result on the run page. Before you can run a prediction, you first need to run the job `select-best-model`. On the run
page you can find the output of the run and also the input, metrics and variables.

You can read more about this demo project in
[the AskAnna documentation](https://docs.askanna.io/examples/train-select-serve-model/).

Using the AskAnna CLI you can run the next command to create a new project in AskAnna with this template:

```bash
askanna create "Train, select and serve" --template https://gitlab.com/askanna/demo/demo-train-select-serve-model.git --push
```
