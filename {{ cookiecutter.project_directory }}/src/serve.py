# Inspiration for the train, select and serve example came from:
# - MLFlow: https://github.com/mlflow/mlflow/blob/master/examples/sklearn_elasticnet_wine/train.py
# - UbiOps: https://github.com/UbiOps/cookbook/tree/master/mlflow-example/mlflow-recipe

import pickle
import os
import sys

import pandas as pd
import askanna


if __name__ == "__main__":
    runs = askanna.run.list(
        job_name="select-best-model",
        number_of_results=10,
        order_by="-created_at",
        include_metrics = True
    )

    if not runs:
        print("There is not a run started for the job 'select-best-model', please run this job first.")
        sys.exit(1)

    train_run_suuid = None
    for run in runs:
        if run.status == "finished":
            train_run_suuid = run.metrics.get("run_suuid").metric.value
            askanna.track_variable(name="model_suuid", value=train_run_suuid)
            print(f"Selected train run: {train_run_suuid}")
            break

    if not train_run_suuid:
        print("There is not a finished run for the job 'select-best-model'. Wait till the job run is finished.")
        sys.exit(1)

    model_data = askanna.result.get(train_run_suuid)
    model = pickle.loads(model_data)

    input_path = os.getenv("AA_PAYLOAD_PATH", "input/dummy-serve-single.json")
    input_data = pd.read_json(input_path)
    askanna.track_metric("input count", len(input_data.index))

    prediction = model.predict(input_data)
    pd.DataFrame(prediction).to_csv("prediction.csv", header=["MPG"], index_label="index")
    askanna.track_metric("output count", len(prediction))
 