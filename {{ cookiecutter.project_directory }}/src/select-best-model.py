# Inspiration for the train, select and serve example came from:
# - MLFlow: https://github.com/mlflow/mlflow/blob/master/examples/sklearn_elasticnet_wine/train.py
# - UbiOps: https://github.com/UbiOps/cookbook/tree/master/mlflow-example/mlflow-recipe

import json
import os
import time

import askanna


if __name__ == "__main__":
    input_path = os.getenv("AA_PAYLOAD_PATH", "input/dummy-select-model.json")
    with open(input_path, "r") as f:
        input_data = json.load(f)

    parameters = input_data.get("parameters", None)

    train_runs = []
    for param in parameters:
        print(f"Running with param = {param}")
        run = askanna.run.start(job_name="train-model", data=param)
        train_runs.append(run.suuid)
        print(f"Status = {run.status}")

    askanna.track_variable("train-runs", train_runs)
    time.sleep(10)

    for run in train_runs:
        runinfo = askanna.run.status(run)
        status = runinfo.status
        while status == "running":
            print(f"Update: {runinfo.suuid} is still running")
            time.sleep(10)
            runinfo = askanna.run.status(run)
            status = runinfo.status

    runs = askanna.run.list(run_suuid_list=train_runs, include_metrics=True, include_variables=True)
    rmse = 1
    for run in runs:
        run_rmse = run.metrics.get("rmse").metric.value
        if run_rmse < rmse:
            run_lowest_rmse = run
            rmse = run_rmse

    alpha = run_lowest_rmse.variables.filter("alpha")[0].variable.value
    l1_ratio = run_lowest_rmse.variables.filter("l1_ratio")[0].variable.value

    print(f"The optimal run SUUID is {run_lowest_rmse.suuid}")
    print(f"It had the parameters: alpha={alpha}, l1_ratio={l1_ratio}")
    print(f"And RMSE: {rmse}")

    askanna.track_metrics(
        {
            "run_suuid": run_lowest_rmse.suuid,
            "alpha": alpha,
            "l1_ratio": l1_ratio,
            "rmse": rmse
        },
        label="best-model"
    )
